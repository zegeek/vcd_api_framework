import requests
import xml.etree.ElementTree as ET
import base64
from elements.organization import Organization
from elements.user import User

class Session:
    apiVersion = "/api/versions"
    def __init__(self,baseURL):
        self.connected = False
        self.version = "0.0"
        self.loginUrl = ""
        get_versions = requests.get(baseURL+__class__.apiVersion)
        if get_versions.status_code != 200:
            raise ConnectionRefusedError('Server responded with {} error'.format(get_versions.status_code))
        else:
            suppVersions = ET.fromstring(get_versions.text)
            for verInfo in suppVersions:
                if verInfo.tag[verInfo.tag.find(('}'))+1::] == "VersionInfo": 
                    if verInfo.attrib['deprecated'] == "false":
                        if int(self.version[0:self.version.find('.')]) < int(verInfo[0].text[0:verInfo[0].text.find('.')]):
                            self.version = verInfo[0].text
                            self.loginUrl = verInfo[1].text
    def loginApi(self,userName,password, orgName=""):
        if len(orgName) > 0:
            self.connectionStr = "{}@{}:{}".format(userName,orgName,password)
        else:
            self.connectionStr = "{}:{}".format(userName,password)
        enc_connectionStr = base64.b64encode(self.connectionStr.encode('ascii'))
        httpHeader = {
            'Accept': 'application/*;version={}'.format(self.version),
            'Authorization':'Basic {}'.format(enc_connectionStr.decode('ascii'))
        }
        get_session = requests.post(self.loginUrl,headers=httpHeader)
        if get_session.status_code != 200:
            raise ConnectionRefusedError('Server responded with {} error'.format(get_versions.status_code))
        else:
            sessionResp = ET.fromstring(get_session.text)
            if sessionResp.tag[sessionResp.tag.find('}')+1::] == "Session":
                self.connected = True
                self.vcloud_token = get_session.headers['x-vcloud-authorization']
                self.curUser = User(sessionResp.attrib['user'],sessionResp.attrib['userId'],sessionResp.attrib['roles'])
    def getSessionHeader(self):
        httpHeader = {}
        if self.connected:
            httpHeader = {
                    'Accept': 'application/*;version={}'.format(self.version),
                    'x-vcloud-authorization':self.vcloud_token
            }
        return httpHeader    
    def getOrgList(self,baseURL):
        if self.connected:
            orgListUrl = "/api/org"
            httpHeader = self.getSessionHeader()
            get_orgList = requests.get(baseURL+orgListUrl,headers=httpHeader)
            self.orgList = []
            if get_orgList.status_code != 200:
                raise ConnectionRefusedError('Cannot retrieve OrgList: Server responded with {} error'.format(get_versions.status_code))
            else:
                orgList = ET.fromstring(get_orgList.text)
                if orgList.tag[orgList.tag.find('}')+1::] == "OrgList":
                    for org in orgList:
                        name = org.attrib['name']
                        apiUrl = org.attrib['href']
                        get_org = requests.get(apiUrl,headers=httpHeader)
                        if get_org.status_code != 200:
                            raise ConnectionRefusedError('Cannot retrieve Org: Server responded with {} error'.format(get_versions.status_code))
                        else:
                            org_xml = ET.fromstring(get_org.text)
                            orgId = org_xml.attrib['id']
                            for childs in org_xml:
                                if childs.tag[childs.tag.find('}')+1::] == 'Description':
                                    description = childs.text
                        orgItem = Organization(name,apiUrl,description,orgId)
                        orgItem.updateSettings(httpHeader)
                        self.orgList.append(orgItem)

    def logoutApi(self):
        if self.connected:
            httpHeader = self.getSessionHeader()
            logout = requests.delete(self.loginUrl,headers=httpHeader)
            if logout.status_code != 200:
                raise ConnectionRefusedError('Logout Error, server responded with {} error'.format(logout.status_code))

    def getApiVersion(self):
        print("API Version: {}".format(self.version))
        print("Login URL: {}".format(self.loginUrl))

    def showSessionToken(self):
        print("x-vcloud-authorization: {}".format(self.vcloud_token))

    def showLoggedUser(self):
        self.curUser.displayInfo()
    
    def showOrgList(self):
        for org in self.orgList:
            org.displayInfo()
            print("===================")
