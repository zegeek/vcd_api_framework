## vCloud Director Python API wrapper
*Description*: Since the organization where I'm working as a system engineer has implemented the vmware cloud solution vCloud Director, I thought it is a good idea to wrap the REST API with a Python interface, for future automation use. this project started ***29/05/2020***.
**Author**: Belha
**Version**: 0.1
**Python version**: Python 3.6.3
**Required modules**:

 - requests
 - xml.etree.ElementTree
 - base64

work in progress..

 - Session object stores the logged in User and it's related organization.
 - Information display regarding organizations

test.py: Python module for usecase and test case.


> Written with [StackEdit](https://stackedit.io/).