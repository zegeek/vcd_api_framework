import requests
import xml.etree.ElementTree as ET

class Organization:
    def __init__(self,name,apiURL,description,orgId):
        self.name = name
        self.apiURL = apiURL
        self.description = description
        self.orgId = orgId[orgId.find('org:')+4::]


    def updateSettings(self,authHeader):
        url = self.apiURL.replace("api/org","api/admin/org")
        url = url+"/settings"
        org_settings = requests.get(url,headers=authHeader)
        if org_settings.status_code != 200:
            raise ConnectionRefusedError("Cannort retrieve OrgSettings, server responded with {} error".format(org_settings.status_code))
        else:
            orgItem = ET.fromstring(org_settings.text)
            if orgItem.tag[orgItem.tag.find('}')+1::] == "OrgSettings":
                for child in orgItem:
                    ############################
                    if child.tag[child.tag.find('}')+1::] == "OrgGeneralSettings":
                        genSettings = {}
                        for sub_child in child:
                            if sub_child.tag[sub_child.tag.find('}')+1::] != "Link":
                                genSettings.update({ sub_child.tag[sub_child.tag.find('}')+1::] : sub_child.text})
                        self.generalSettings = genSettings
                    ############################
                    if child.tag[child.tag.find('}')+1::] == "VAppLeaseSettings":
                        vappLeaseSettings = {}
                        for sub_child in child:
                            if sub_child.tag[sub_child.tag.find('}')+1::] != "Link":
                                vappLeaseSettings.update({ sub_child.tag[sub_child.tag.find('}')+1::] : sub_child.text})
                        self.vappLeaseSettings = vappLeaseSettings
                    ############################
                    if child.tag[child.tag.find('}')+1::] == "VAppTemplateLeaseSettings":
                        vappTemplateLeaseSettings = {}
                        for sub_child in child:
                            if sub_child.tag[sub_child.tag.find('}')+1::] != "Link":
                                vappTemplateLeaseSettings.update({ sub_child.tag[sub_child.tag.find('}')+1::] : sub_child.text})
                        self.vappTemplateLeaseSettings = vappTemplateLeaseSettings
                    ############################
                    if child.tag[child.tag.find('}')+1::] == "OrgLdapSettings":
                        orgLdapSettings = {}
                        for sub_child in child:
                            if sub_child.tag[sub_child.tag.find('}')+1::] != "Link":
                                orgLdapSettings.update({ sub_child.tag[sub_child.tag.find('}')+1::] : sub_child.text})
                        self.orgLdapSettings = orgLdapSettings
                    ############################
                    if child.tag[child.tag.find('}')+1::] == "OrgEmailSettings":
                        orgEmailSettings = {}
                        for sub_child in child:
                            if sub_child.tag[sub_child.tag.find('}')+1::] != "Link":
                                if sub_child.tag[sub_child.tag.find('}')+1::] != "SmtpServerSettings":
                                    orgEmailSettings.update({ sub_child.tag[sub_child.tag.find('}')+1::] : sub_child.text})
                                else:
                                    smtpSrvStg = {}
                                    for sub_sub_child in sub_child:
                                        if sub_sub_child.tag[sub_child.tag.find('}')+1::] != "Link":
                                            smtpSrvStg.update({sub_sub_child.tag[sub_sub_child.tag.find('}')+1::] : sub_sub_child.text})
                                    orgEmailSettings.update({ sub_child.tag[sub_child.tag.find('}')+1::] : smtpSrvStg})
                        self.orgEmailSettings = orgEmailSettings
                    ############################
                    if child.tag[child.tag.find('}')+1::] == "OrgPasswordPolicySettings":
                        orgPasswordPolicySettings = {}
                        for sub_child in child:
                            if sub_child.tag[sub_child.tag.find('}')+1::] != "Link":
                                orgPasswordPolicySettings.update({ sub_child.tag[sub_child.tag.find('}')+1::] : sub_child.text})
                        self.orgPasswordPolicySettings = orgPasswordPolicySettings
                    ############################
                    if child.tag[child.tag.find('}')+1::] == "OrgOperationLimitsSettings":
                        orgOperationLimitsSettings = {}
                        for sub_child in child:
                            if sub_child.tag[sub_child.tag.find('}')+1::] != "Link":
                                orgOperationLimitsSettings.update({ sub_child.tag[sub_child.tag.find('}')+1::] : sub_child.text})
                        self.orgOperationLimitsSettings = orgOperationLimitsSettings
                    ############################
                    if child.tag[child.tag.find('}')+1::] == "OrgFederationSettings":
                        orgFederationSettings = {}
                        for sub_child in child:
                            if sub_child.tag[sub_child.tag.find('}')+1::] != "Link":
                                orgFederationSettings.update({ sub_child.tag[sub_child.tag.find('}')+1::] : sub_child.text})
                        self.orgFederationSettings = orgFederationSettings
                    ############################
                    if child.tag[child.tag.find('}')+1::] == "OrgOAuthSettings":
                        orgOAuthSettings = {}
                        for sub_child in child:
                            if sub_child.tag[sub_child.tag.find('}')+1::] != "Link":
                                orgOAuthSettings.update({ sub_child.tag[sub_child.tag.find('}')+1::] : sub_child.text})
                        self.orgOAuthSettings = orgOAuthSettings

    def displayGeneralSettings(self):
        print("General Settings: {}".format(self.generalSettings))
    def displayVAppLeaseSettings(self):
        print("VApp Lease Settings: {}".format(self.vappLeaseSettings))
    def displayVAppTemplateLeaseSettings(self):
        print("VApp Template Lease Settings: {}".format(self.vappTemplateLeaseSettings))
    def displayOrgLdapSettings(self):
        print("Ldap Settings: {}".format(self.orgLdapSettings))
    def displayOrgEmailSettings(self):
        print("Email Settings: {}".format(self.orgEmailSettings))
    def displayOrgPasswordPolicySettings(self):
        print("Password Policy Settings: {}".format(self.orgPasswordPolicySettings))
    def displayOrgOperationLimitsSettings(self):
        print("Operation Limits Settings: {}".format(self.orgOperationLimitsSettings))
    def displayOrgFederationSettings(self):
        print("Federation Settings: {}".format(self.orgFederationSettings))
    def displayOrgOAuthSettings(self):
        print("OAuth Settings: {}".format(self.orgOAuthSettings))
    
    def displayInfo(self):
        print("Organization name: {}\nOrganization description: {}\nOrgID: {}\nURL: {}".format(self.name,self.description,self.orgId, self.apiURL))
        self.displayGeneralSettings()
        self.displayVAppLeaseSettings()
        self.displayVAppTemplateLeaseSettings()
        self.displayOrgLdapSettings()
        self.displayOrgEmailSettings()
        self.displayOrgPasswordPolicySettings()
        self.displayOrgOperationLimitsSettings()
        self.displayOrgFederationSettings()
        self.displayOrgOAuthSettings()